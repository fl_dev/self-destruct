package config

type Conf struct {
	Domain        string `long:"domain" env:"DOMAIN" default:"localhost:8081" description:"the domain of the project"`
	Listen        string `long:"listen" env:"LISTEN" default:":8081" description:"the ip & port to listen"`
	MetricsListen string `long:"metrics_listen" env:"METRICS_LISTEN" default:":9090" description:"the ip & port to listen for metrics scrapes"`
}

var App = Conf{}
