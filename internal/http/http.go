package http

import (
	"embed"
	"errors"
	"fmt"
	"html/template"
	"net/http"
	ttemplate "text/template"

	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	"github.com/labstack/gommon/log"
	. "self-destruct.zip/internal/errors"
	"self-destruct.zip/internal/logic"
	"self-destruct.zip/internal/types"
)

//go:embed images
var imageFiles embed.FS

//go:embed templates/*.html
var content embed.FS

var tmpl *template.Template

type handlers struct {
	l logic.ILogic
}

func GenerateDestroyInDropdown(selected string) template.HTML {
	options := [][]string{
		{"168h", "7 days"},
		{"24h", "1 day"},
		{"12h", "12 hours"},
		{"6h", "6 hours"},
	}

	var s string
	// look away
	for _, vals := range options {
		val := vals[0]
		title := vals[1]
		selectedText := ""
		if val == selected {
			selectedText = "selected"
		}
		s += fmt.Sprintf(`<option value="%s" %s>%s</option>`, val, selectedText, title)
	}

	return template.HTML(s)
}

func ReadTemplates() {
	var err error
	tmpl, err = template.New("").Funcs(ttemplate.FuncMap{
		"DestroyInDropdown": GenerateDestroyInDropdown,
	}).ParseFS(content, "templates/*.html")
	if err != nil {
		panic(err)
	}
}

func NewServer(l logic.ILogic) *echo.Echo {
	ReadTemplates()
	e := echo.New()
	e.Use(middleware.Logger())
	e.Use(middleware.StaticWithConfig(middleware.StaticConfig{
		Root:       "images",
		Filesystem: http.FS(imageFiles),
	}))
	h := newHandlers(l)
	e.POST("/message", h.StoreMessage)
	e.POST("/message/decrypt", h.DecryptMessage)
	e.GET("/", h.Index)
	e.GET("/:message_path", h.GetMessage)
	return e
}

func (h *handlers) Index(ctx echo.Context) error {
	return renderPage("form.html", ctx.Response().Writer, nil)
}

func newHandlers(l logic.ILogic) *handlers {
	return &handlers{
		l: l,
	}
}

func (h *handlers) GetMessage(ctx echo.Context) error {
	w := ctx.Response().Writer
	path := ctx.Param("message_path")
	message, err := h.l.FetchMessage(ctx.Request().Context(), path)
	if err != nil || message == nil {
		return renderErrorPage(w, err)
	}
	if message.Encrypted == types.EncryptedOn {
		data := FormData{
			Values: map[string]string{
				"path": path,
			},
		}
		return renderPage("encrypted.html", w, data)
	}

	return renderPage("message.html", w, message)
}

type FormData struct {
	Values map[string]string
	Errors map[string]string
}

func (h *handlers) DecryptMessage(ctx echo.Context) error {
	w := ctx.Response().Writer
	decryptRequest := new(types.DecryptRequest)
	if err := ctx.Bind(decryptRequest); err != nil {
		return renderErrorContent(w, err)
	}

	message, err := h.l.DecryptMessage(ctx.Request().Context(), decryptRequest)
	if err != nil && errors.Is(err, ErrInvalidPassword) {
		data := FormData{
			Values: map[string]string{
				"path": decryptRequest.Path,
			},
			Errors: map[string]string{
				"password": "Failed to decrypt, try again",
			},
		}
		return renderContent("encrypted.html", w, data)
	}
	if err != nil {
		return renderErrorContent(w, err)
	}

	return renderContent("message.html", w, message)
}

func (h *handlers) StoreMessage(ctx echo.Context) error {
	w := ctx.Response().Writer
	message := new(types.Message)
	if err := ctx.Bind(message); err != nil {
		return renderErrorContent(w, err)
	}
	if err := message.Validate(); err != nil {
		data := FormData{
			Values: map[string]string{
				"burnAfterReading": string(message.BurnAfterReading),
				"encrypted":        string(message.Encrypted),
				"expiry":           message.Expiry,
				"body":             message.Body,
			},
			Errors: map[string]string{
				"submission": err.Error(),
			},
		}

		return renderContent("form.html", w, data)
	}
	link, err := h.l.StoreMessage(ctx.Request().Context(), message)
	if err != nil {
		return renderErrorContent(w, err)
	}

	return renderContent("link.html", w, link)
}

func renderContent(name string, w http.ResponseWriter, data interface{}) error {
	return render(name, w, data, "content")
}

func renderPage(name string, w http.ResponseWriter, data interface{}) error {
	return render(name, w, data, "page")
}

// render function clones the templates, and re-reads the desired template so that it overwrites the content block
// lets us re-use the layout without lots of logic in the template.
// https://stackoverflow.com/questions/36617949/how-to-use-base-template-file-for-golang-html-template
func render(name string, w http.ResponseWriter, data interface{}, renderType string) error {
	clone := template.Must(tmpl.Clone())
	clone = template.Must(clone.ParseFS(content, fmt.Sprintf("templates/%s", name)))
	err := clone.Lookup(fmt.Sprintf("%s.html", renderType)).Execute(w, data)
	if err != nil {
		return renderError(w, err, renderType)
	}

	return nil
}

func renderErrorContent(w http.ResponseWriter, err error) error {
	return renderError(w, err, "content")
}

func renderErrorPage(w http.ResponseWriter, err error) error {
	return renderError(w, err, "page")
}

func renderError(w http.ResponseWriter, err error, renderType string) error {
	log.Error(err)
	clone := template.Must(tmpl.Clone())
	clone = template.Must(clone.ParseFS(content, fmt.Sprintf("templates/%s.html", "error")))
	var withStatusError *WithStatusErr
	errors.As(err, &withStatusError)

	return clone.Lookup(fmt.Sprintf("%s.html", renderType)).Execute(w, withStatusError)

}
