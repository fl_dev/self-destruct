package types

import (
	"fmt"
	"time"

	"self-destruct.zip/internal/errors"
)

type BurnAfterReadingOption string

const (
	BurnAfterReadingOn  BurnAfterReadingOption = "on"
	BurnAfterReadingOff BurnAfterReadingOption = "off"
)

type EncryptedOption string

const (
	EncryptedOn  EncryptedOption = "on"
	EncryptedOff EncryptedOption = "off"
)

const (
	Expired            = "EXPIRED"
	BurnedAfterReading = "BURNED_AFTER_READING"
)

type Link struct {
	URL string
}

type EncryptedMessage struct {
	EncryptedText string
	Nonce         string
	Salt          string
}

type DecryptRequest struct {
	Path     string `form:"path"`
	Password string `form:"password"`
}

type Message struct {
	Body             string                 `form:"body"`
	BurnAfterReading BurnAfterReadingOption `form:"burnAfterReading"`
	Expiry           string                 `form:"expiry"`
	Password         string                 `form:"password"`
	Encrypted        EncryptedOption        `form:"encrypt"`
	EncryptedMessage *EncryptedMessage

	// After a message is destroyed, a TTL (timestamp) is added so that any
	// attempts to re-read that message will be notified that it is destroyed.
	Destroyed        bool
	DestroyedBecause *string
	RemoveAfter      *int64

	CreatedAt int64
}

func (m *Message) Copy() *Message {
	var removeAfter int64
	if m.RemoveAfter != nil {
		removeAfter = *m.RemoveAfter
	}
	var destroyedBecause string
	if m.DestroyedBecause != nil {
		destroyedBecause = *m.DestroyedBecause
	}

	var eMessage EncryptedMessage
	if m.EncryptedMessage != nil {
		eMessage = EncryptedMessage{
			Salt:          m.EncryptedMessage.Salt,
			Nonce:         m.EncryptedMessage.Nonce,
			EncryptedText: m.EncryptedMessage.EncryptedText,
		}
	}

	return &Message{
		Body:             m.Body,
		BurnAfterReading: m.BurnAfterReading,
		Expiry:           m.Expiry,
		Destroyed:        m.Destroyed,
		RemoveAfter:      &removeAfter,
		CreatedAt:        m.CreatedAt,
		DestroyedBecause: &destroyedBecause,
		Encrypted:        m.Encrypted,
		EncryptedMessage: &eMessage,
	}
}

func (m Message) Validate() error {
	dur, err := time.ParseDuration(m.Expiry)
	if err != nil {
		return errors.ErrInvalidExpiry
	}
	if dur > (time.Duration(30 * 24 * time.Hour)) {
		return errors.ErrExpiryTooLong
	}

	if len(m.Body) > 5000 {
		return fmt.Errorf("Message too long")
	}

	if len(m.Body) == 0 {
		return fmt.Errorf("Message content empty")
	}

	if m.Encrypted == EncryptedOn && len(m.Password) < 3 {
		return fmt.Errorf("Password must be greater than 3 characters")
	}
	return nil
}
