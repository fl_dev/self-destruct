package persistence

import (
	. "self-destruct.zip/internal/types"
)

type IPersist interface {
	GetMessage(path string) (*Message, error)
	StoreMessage(path string, message *Message) error
	DestroyExpiredMessages() error
	DestroyMessages(paths []string) error
	RemoveDestroyedMessages() error
}
