package errors

import "fmt"

type WithStatusErr struct {
	Message string
	Code    int
}

func (e *WithStatusErr) Error() string {
	return e.Message
}

func NewWithStatusError(m string, c int) *WithStatusErr {
	return &WithStatusErr{
		Message: m,
		Code:    c,
	}
}

var ErrPathExists = NewWithStatusError("Path already exists", 500)
var ErrMessageNotFound = NewWithStatusError("Message not found", 400)
var ErrMessageExpired = NewWithStatusError("This message has expired and can no longer be read", 400)
var ErrMessageDestroyed = NewWithStatusError("This message has already been read and has been destroyed", 400)
var ErrMessageNoLongerExists = NewWithStatusError("This message no longer exists", 400)
var ErrInvalidMessage = NewWithStatusError("Invalid message content", 400)
var ErrExpiryTooLong = NewWithStatusError("Expiry too long", 500)
var ErrInvalidExpiry = NewWithStatusError("Invalid expiry", 500)

var ErrInvalidPassword = fmt.Errorf("Invalid password provided")
