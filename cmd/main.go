package main

import (
	"context"
	"log"
	"time"

	"github.com/jessevdk/go-flags"
	"golang.org/x/sync/errgroup"

	. "self-destruct.zip/cmd/config"
	"self-destruct.zip/internal/cleanup"
	"self-destruct.zip/internal/generate"
	"self-destruct.zip/internal/http"
	"self-destruct.zip/internal/logic"
	"self-destruct.zip/internal/metrics"
	"self-destruct.zip/internal/persistence"
)

func main() {
	parser := flags.NewParser(&App, flags.Default)
	if _, err := parser.Parse(); err != nil {
		log.Fatal(err)
	}
	g, err := generate.NewGenerator()
	if err != nil {
		log.Fatal(err)
	}
	p := persistence.NewInMemStore()
	m := metrics.NewMetrics()
	logicArgs := logic.LogicArgs{
		Persistence: p,
		Domain:      App.Domain,
		Gen:         g,
		Metrics:     m,
	}
	l := logic.NewLogic(logicArgs)
	s := http.NewServer(l)

	c := cleanup.NewClient(p, cleanup.Conf{
		Interval: time.Minute,
	})

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	go c.Start(ctx)

	group := errgroup.Group{}
	group.Go(func() error {
		return s.Start(App.Listen)
	})
	group.Go(func() error {
		return m.ServeMetrics(App.MetricsListen)
	})
	log.Fatal(group.Wait())
}
