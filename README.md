# SelfDestruct.zip

https://selfdestruct.zip

![](assets/demo.mov)

## Background
This is a tool that I build to help me share text between devices. Heavily inspired by privnote.com but with memorable paths in the generated URL. 
You have the option for the message to be deleted after it is read once, or after a set amount of time. 
You also have the option to encrypt the message.

## Running the application locally

### Pre-requisites
- [golang](https://go.dev/) installed
- [air](https://github.com/air-verse/air) installed (optional for hot reloading)

### Running the application
If you have air installed
`air`
Otherwise
`go run cmd/main.go`

## Notes

### HTMX
I've gone out of my way not to add any javascript at all, with the help of [HTMX](https://htmx.org/). No particular reason other than to see what I could do, without explicitly writing any JS. (Which means I couldn't add a "copy to clipboard" button beside the generated url). 

### Persistence
This is a lightweight tool for ephemeral messages, so I've not bothered persisting the data to disk. Which means that any messages stored can be wiped at any time. 

### Hosting
The service & gitlab runner are both hosted on my home server which is likely to be up and down. 

### Encryption
Messages are encrypted on the backend, with AES encryption. The encrypted message, salt and nonce are stored to decrypt the message with the password provided. This was another tradeoff from not using javascript, as I am a fan of zero knowledge tools. 

### Brute forcing
Given the URL path is comprised of an adverb, adjective & noun to make it easier to remember, it is also much easier to brute force messages, so encrypting messages is advised. 

### Metrics
I capture some basic metrics when messages are created/read, which get scraped by my prometheus instance. 

![](assets/metrics.png)
