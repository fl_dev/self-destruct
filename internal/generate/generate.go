package generate

import (
	"crypto/aes"
	"crypto/cipher"
	"crypto/sha256"
	_ "embed"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"math/rand"
	"time"

	. "self-destruct.zip/internal/errors"
	. "self-destruct.zip/internal/types"
)

//go:embed adverbs.json
var adverbsJson string

//go:embed adjectives.json
var adjectivesJson string

//go:embed nouns.json
var nounsJson string

type generator struct {
	nouns      []string
	adverbs    []string
	adjectives []string
}

type IGenerator interface {
	ReadablePath(...func(string) string) string
	Hash(text, salt []byte) ([]byte, error)
	GenerateRandomBytes(size int) ([]byte, error)
	Encrypt(plainText, password string) (*EncryptedMessage, error)
	Decrypt(password string, emessage EncryptedMessage) (string, error)
}

func NewGenerator() (IGenerator, error) {
	gen := &generator{}
	nouns, err := unmarshalWordArray(nounsJson)
	if err != nil {
		return gen, fmt.Errorf("failed to unmarshal nouns file with err: %w", err)
	}
	adverbs, err := unmarshalWordArray(adverbsJson)
	if err != nil {
		return gen, fmt.Errorf("failed to unmarshal adverbs file with err: %w", err)
	}
	adjectives, err := unmarshalWordArray(adjectivesJson)
	if err != nil {
		return gen, fmt.Errorf("failed to unmarshal adjectives file with err: %w", err)
	}
	gen.adjectives = adjectives
	gen.adverbs = adverbs
	gen.nouns = nouns

	// only needs to be seeded once
	rand.Seed(time.Now().UnixNano())

	return gen, nil
}

func unmarshalWordArray(embeddedJson string) ([]string, error) {
	var wordArr []string
	err := json.Unmarshal([]byte(embeddedJson), &wordArr)
	return wordArr, err
}

func (g *generator) Decrypt(password string, emessage EncryptedMessage) (string, error) {
	salt, err := base64.StdEncoding.DecodeString(emessage.Salt)
	if err != nil {
		return "", err
	}

	nonce, err := base64.StdEncoding.DecodeString(emessage.Nonce)
	if err != nil {
		return "", err
	}

	key, err := g.Hash([]byte(password), salt)
	if err != nil {
		return "", err
	}

	block, err := aes.NewCipher(key)
	if err != nil {
		return "", err
	}

	gcm, err := cipher.NewGCM(block)
	if err != nil {
		return "", err
	}

	cipherTextBytes, err := base64.StdEncoding.DecodeString(emessage.EncryptedText)
	if err != nil {
		return "", err
	}

	plainText, err := gcm.Open(nil, nonce, cipherTextBytes, nil)
	if err != nil {
		// assuming all errors from this function are incorrect password errors which is probably incorrect, but I'm lazy
		return "", ErrInvalidPassword
	}

	return string(plainText), nil
}

func (g *generator) Encrypt(plainText, password string) (*EncryptedMessage, error) {
	salt, err := g.GenerateRandomBytes(16)
	if err != nil {
		return nil, fmt.Errorf("error generating salt: %w", err)
	}

	key, err := g.Hash([]byte(password), []byte(salt))
	if err != nil {
		return nil, fmt.Errorf("error hashing password: %w", err)
	}

	block, err := aes.NewCipher(key)
	if err != nil {
		return nil, fmt.Errorf("error creating aes cypher block: %w", err)
	}

	gcm, err := cipher.NewGCM(block)
	if err != nil {
		return nil, fmt.Errorf("error creating GCM: %w", err)
	}

	nonce, err := g.GenerateRandomBytes(gcm.NonceSize())
	if err != nil {
		return nil, fmt.Errorf("error generating nonce: %w", err)
	}

	cipherText := gcm.Seal(nil, nonce, []byte(plainText), nil)

	// Return the encrypted message, salt, and nonce as base64-encoded strings
	return &EncryptedMessage{
		EncryptedText: base64.StdEncoding.EncodeToString(cipherText),
		Salt:          base64.StdEncoding.EncodeToString(salt),
		Nonce:         base64.StdEncoding.EncodeToString(nonce),
	}, nil
}

func (g *generator) Hash(text, salt []byte) ([]byte, error) {
	hasher := sha256.New()
	hasher.Write(salt)
	hasher.Write(text)
	return hasher.Sum(nil), nil
}

func (g *generator) GenerateRandomBytes(size int) ([]byte, error) {
	bytes := make([]byte, size)
	_, err := rand.Read(bytes)
	if err != nil {
		return nil, err
	}
	return bytes, nil
}

func (g *generator) ReadablePath(options ...func(string) string) string {
	noun := getRandomElement(g.nouns)
	adjective := getRandomElement(g.adjectives)
	adverb := getRandomElement(g.adverbs)

	path := fmt.Sprintf("%s-%s-%s", adverb, adjective, noun)
	for _, option := range options {
		path = option(path)
	}
	return path
}

func getRandomElement[T any](arr []T) T {
	randomIndex := rand.Intn(len(arr))
	return arr[randomIndex]
}
