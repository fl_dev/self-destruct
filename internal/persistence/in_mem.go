package persistence

import (
	"strings"
	"sync"
	"time"

	. "self-destruct.zip/internal/errors"
	. "self-destruct.zip/internal/types"
)

type inMemStore struct {
	messages map[string]*Message
	mutex    sync.Mutex
}

func NewInMemStore() IPersist {
	return &inMemStore{
		messages: make(map[string]*Message),
		mutex:    sync.Mutex{},
	}
}

func (s *inMemStore) GetMessage(path string) (*Message, error) {
	s.mutex.Lock()
	defer s.mutex.Unlock()
	message, ok := s.messages[strings.TrimSpace(path)]
	if !ok {
		return nil, ErrMessageNotFound
	}

	return message.Copy(), nil
}

func (s *inMemStore) StoreMessage(path string, message *Message) error {
	s.mutex.Lock()
	defer s.mutex.Unlock()

	if _, ok := s.messages[path]; ok {
		return ErrPathExists
	}

	s.messages[path] = message
	return nil
}

func (s *inMemStore) DestroyMessages(paths []string) error {
	s.mutex.Lock()
	defer s.mutex.Unlock()
	for _, path := range paths {
		message, ok := s.messages[path]
		if !ok {
			return ErrMessageNotFound
		}
		markMessageAsDestroyed(message, BurnedAfterReading)
	}

	return nil
}

func (s *inMemStore) DestroyExpiredMessages() error {
	s.mutex.Lock()
	defer s.mutex.Unlock()

	now := time.Now().Unix()
	for _, message := range s.messages {
		dur, err := time.ParseDuration(message.Expiry)
		if err != nil {
			return ErrInvalidExpiry
		}
		expiry := message.CreatedAt + int64(dur.Seconds())
		if now > expiry {
			markMessageAsDestroyed(message, Expired)
		}
	}

	return nil
}

func (s *inMemStore) RemoveDestroyedMessages() error {
	s.mutex.Lock()
	defer s.mutex.Unlock()

	pathsForRemoval := []string{}
	now := time.Now().Unix()
	for path, message := range s.messages {
		if message.Destroyed && message.RemoveAfter != nil && now > *message.RemoveAfter {
			pathsForRemoval = append(pathsForRemoval, path)
		}
	}

	for _, path := range pathsForRemoval {
		delete(s.messages, path)
	}

	return nil
}

func markMessageAsDestroyed(message *Message, reason string) *Message {
	destroyedMessageTTL := time.Now().Add(time.Hour * 24).Unix()
	message.Body = ""
	message.Destroyed = true
	message.RemoveAfter = &destroyedMessageTTL
	message.DestroyedBecause = &reason

	return message
}
