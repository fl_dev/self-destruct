package cleanup

import (
	"context"
	"fmt"
	"log"
	"time"

	"self-destruct.zip/internal/persistence"
)

type client struct {
	persist persistence.IPersist
	log     log.Logger
	conf    Conf
}

type Conf struct {
	Interval time.Duration
}

func NewClient(p persistence.IPersist, conf Conf) *client {
	return &client{
		persist: p,
		log:     *log.Default(),
		conf:    conf,
	}
}

func (c *client) Start(ctx context.Context) {
	ticker := time.NewTicker(c.conf.Interval)
	defer ticker.Stop()

	for {
		select {
		case <-ctx.Done():
			return
		case <-ticker.C:
			c.clean()
		}
	}
}

func (c *client) clean() {
	fmt.Println("destroying messages")
	if err := c.persist.DestroyExpiredMessages(); err != nil {
		c.log.Printf("error destroying expired messages:  %s", err.Error())
	}
	if err := c.persist.RemoveDestroyedMessages(); err != nil {
		c.log.Printf("error removing destroyed messages:  %s", err.Error())
	}
}
