package logic

import (
	"context"
	"errors"
	"fmt"
	"time"

	. "self-destruct.zip/internal/errors"
	"self-destruct.zip/internal/generate"
	"self-destruct.zip/internal/metrics"
	"self-destruct.zip/internal/persistence"
	"self-destruct.zip/internal/types"
)

type ILogic interface {
	FetchMessage(ctx context.Context, path string) (*types.Message, error)
	StoreMessage(ctx context.Context, message *types.Message) (*types.Link, error)
	DecryptMessage(ctx context.Context, decryptRequest *types.DecryptRequest) (*types.Message, error)
}

type LogicArgs struct {
	Gen         generate.IGenerator
	Persistence persistence.IPersist
	Metrics     metrics.IMetrics
	Domain      string
}

func NewLogic(args LogicArgs) ILogic {
	return &Logic{
		gen:     args.Gen,
		persist: args.Persistence,
		domain:  args.Domain,
		metrics: args.Metrics,
	}
}

type Logic struct {
	persist persistence.IPersist
	gen     generate.IGenerator
	domain  string
	metrics metrics.IMetrics
}

func (l *Logic) DecryptMessage(ctx context.Context, decryptRequest *types.DecryptRequest) (*types.Message, error) {
	message, err := l.persist.GetMessage(decryptRequest.Path)
	if err != nil {
		if errors.Is(err, ErrMessageNotFound) {
			l.metrics.MessageReadInc(metrics.MessageReadNotFound)
		}
		return nil, fmt.Errorf("failed to get message: %w", err)
	}

	if err := l.validateMessageNotDestroyed(message); err != nil {
		return nil, err
	}

	if message.EncryptedMessage == nil {
		return nil, fmt.Errorf("decrypt api called but no encrypted text found in message object")
	}

	dmessage, err := l.gen.Decrypt(decryptRequest.Password, *message.EncryptedMessage)
	if err != nil {
		if errors.Is(err, ErrInvalidPassword) {
			l.metrics.MessageReadInc(metrics.MessageReadInvalidPassword)
		}
		return nil, err
	}
	message.Body = dmessage

	// if burnAfterReading enabled, destroy the message
	if message.BurnAfterReading == types.BurnAfterReadingOn {
		if err := l.persist.DestroyMessages([]string{decryptRequest.Path}); err != nil {
			return nil, fmt.Errorf("failed to burn message after reading: %w", err)
		}
	}

	l.metrics.MessageReadInc(metrics.MessageReadSuccess)
	return message, nil
}

func (l *Logic) validateMessageNotDestroyed(message *types.Message) error {
	switch {
	case message == nil:
		l.metrics.MessageReadInc(metrics.MessageReadNotFound)
		return ErrMessageNoLongerExists
	case message.Destroyed && *message.DestroyedBecause == string(types.Expired):
		l.metrics.MessageReadInc(metrics.MessageReadExpired)
		return ErrMessageExpired
	case message.Destroyed:
		l.metrics.MessageReadInc(metrics.MessageReadDestroyed)
		return ErrMessageDestroyed
	default:
		return nil
	}
}

func (l *Logic) FetchMessage(ctx context.Context, path string) (*types.Message, error) {
	message, err := l.persist.GetMessage(path)
	if err != nil {
		if errors.Is(err, ErrMessageNotFound) {
			l.metrics.MessageReadInc(metrics.MessageReadNotFound)
		}

		return nil, fmt.Errorf("failed to get message: %w", err)
	}

	if err := l.validateMessageNotDestroyed(message); err != nil {
		return nil, err
	}

	// if encrypted, return the message immediately
	if message.Encrypted == types.EncryptedOn {
		l.metrics.MessageReadInc(metrics.MessageReadEncrypted)
		return message, nil
	}

	// if burnAfterReading enabled, destroy the message
	if message.BurnAfterReading == types.BurnAfterReadingOn {
		if err := l.persist.DestroyMessages([]string{path}); err != nil {
			return nil, fmt.Errorf("failed to burn message after reading: %w", err)
		}
	}
	l.metrics.MessageReadInc(metrics.MessageReadSuccess)
	return message, nil
}

func (l *Logic) StoreMessage(ctx context.Context, message *types.Message) (*types.Link, error) {
	l.metrics.MessageCreatedInc(string(message.Encrypted), message.Expiry, string(message.BurnAfterReading))
	if message.Encrypted == types.EncryptedOn {
		eMessage, err := l.gen.Encrypt(message.Body, message.Password)
		if err != nil {
			return nil, fmt.Errorf("failed to encrypt message: %w", err)
		}
		message.Body = ""
		message.Password = ""
		message.EncryptedMessage = eMessage
	}
	// reset outside the encrypt "on" block in case encrypt was false but the password was still sent
	message.Password = ""
	path := l.gen.ReadablePath()
	checkPathExists := true
	for checkPathExists {
		if _, err := l.persist.GetMessage(path); err != nil {
			if errors.Is(err, ErrMessageNotFound) {
				checkPathExists = false
				continue
			}

			return nil, fmt.Errorf("error checking for path collisions: %w", err)
		}

		path = l.gen.ReadablePath()
	}

	message.CreatedAt = time.Now().Unix()
	message.Destroyed = false

	err := l.persist.StoreMessage(path, message)

	return &types.Link{URL: fmt.Sprintf("%s/%s", l.domain, path)}, err
}
