package metrics

import (
	"github.com/labstack/echo-contrib/echoprometheus"
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
)

var (
	promNamespace             = "self_destruct"
	promLabelEncrypted        = "encrypted"
	promLabelBurnAfterReading = "burn_after_reading"
	promLabelExpiry           = "expiry"
	promLabelStatus           = "status"
)

type IMetrics interface {
	MessageReadInc(status MessageReadStatus)
	MessageCreatedInc(encrypted, expiry, burnAfterReading string)
	ServeMetrics(address string) error
}

type metrics struct {
	registry              *prometheus.Registry
	messageCreatedCounter *prometheus.CounterVec
	messageReadsCounter   *prometheus.CounterVec
}

func NewMetrics() IMetrics {
	r := prometheus.NewRegistry()
	return &metrics{
		registry: r,
		messageCreatedCounter: promauto.With(r).NewCounterVec(prometheus.CounterOpts{
			Namespace: promNamespace,
			Name:      "message_created_counter",
		}, []string{promLabelEncrypted, promLabelExpiry, promLabelBurnAfterReading}),
		messageReadsCounter: promauto.With(r).NewCounterVec(prometheus.CounterOpts{
			Namespace: promNamespace,
			Name:      "message_read_counter",
		}, []string{promLabelStatus}),
	}
}

func (m *metrics) ServeMetrics(address string) error {
	e := echo.New()
	e.Use(middleware.Logger())
	e.GET("/metrics", echoprometheus.NewHandlerWithConfig(echoprometheus.HandlerConfig{Gatherer: m.registry}))

	return e.Start(address)
}

func (m *metrics) MessageCreatedInc(encrypted, expiry, burnAfterReading string) {
	m.messageCreatedCounter.With(prometheus.Labels{
		promLabelBurnAfterReading: burnAfterReading,
		promLabelExpiry:           expiry,
		promLabelEncrypted:        encrypted,
	}).Inc()
}

type MessageReadStatus string

const (
	MessageReadSuccess         MessageReadStatus = "success"
	MessageReadDestroyed       MessageReadStatus = "destroyed"
	MessageReadExpired         MessageReadStatus = "expired"
	MessageReadNotFound        MessageReadStatus = "not_found"
	MessageReadEncrypted       MessageReadStatus = "encrypted"
	MessageReadInvalidPassword MessageReadStatus = "invalid_password"
)

func (m *metrics) MessageReadInc(status MessageReadStatus) {
	m.messageReadsCounter.With(prometheus.Labels{
		promLabelStatus: string(status),
	}).Inc()
}
